/*****************************************************************************

Filename    :   main.cpp
Content     :   Simple minimal VR demo
Created     :   December 1, 2014
Author      :   Tom Heath
Copyright   :   Copyright 2012 Oculus, Inc. All Rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

/*****************************************************************************/
/// This sample has not yet been fully assimiliated into the framework
/// and also the GL support is not quite fully there yet, hence the VR
/// is not that great!

#include "../../OculusRoomTiny_Advanced/Common/Win32_GLAppUtil.h"

// Include the Oculus SDK
#include "OVR_CAPI_GL.h"
#include "Extras/OVR_Math.h"

#include "NET_worlds_console_VR.h"

#if defined(_WIN32)
    #include <dxgi.h> // for GetDefaultAdapterLuid
    #pragma comment(lib, "dxgi.lib")
#endif


using namespace OVR;


static ovrGraphicsLuid GetDefaultAdapterLuid()
{
    ovrGraphicsLuid luid = ovrGraphicsLuid();

    #if defined(_WIN32)
        IDXGIFactory* factory = nullptr;

        if (SUCCEEDED(CreateDXGIFactory(IID_PPV_ARGS(&factory))))
        {
            IDXGIAdapter* adapter = nullptr;

            if (SUCCEEDED(factory->EnumAdapters(0, &adapter)))
            {
                DXGI_ADAPTER_DESC desc;

                adapter->GetDesc(&desc);
                memcpy(&luid, &desc.AdapterLuid, sizeof(luid));
                adapter->Release();
            }

            factory->Release();
        }
    #endif

    return luid;
}


static int Compare(const ovrGraphicsLuid& lhs, const ovrGraphicsLuid& rhs)
{
    return memcmp(&lhs, &rhs, sizeof(ovrGraphicsLuid));
}

void CaptureAnImage(HWND hwnd, ovrRecti * viewport, ovrFovPort * fov);

TextureBuffer * eyeRenderTexture[2] = { nullptr, nullptr };
DepthBuffer   * eyeDepthBuffer[2] = { nullptr, nullptr };
ovrMirrorTexture mirrorTexture = nullptr;
GLuint          mirrorFBO = 0;
ovrSession session;
ovrGraphicsLuid luid;
long long frameIndex = 0;
double sensorSampleTime;    // sensorSampleTime is fed into the layer later
ovrPosef EyeRenderPose[2];
double SCALE = 100.0F;


JNIEXPORT void JNICALL Java_NET_worlds_console_VR_vrInit(JNIEnv * env, jclass clazz, jint scale) {
	SCALE = (double)scale;

	jclass transformClass = env->FindClass("NET/worlds/scape/Transform");
	jmethodID transformConstructor = env->GetMethodID(transformClass, "<init>", "()V");
	jobject leftEyeTransform = env->NewObject(transformClass, transformConstructor);
	jfieldID leftEyeTransformFieldID = env->GetStaticFieldID(clazz, "leftEyeTransform", "LNET/worlds/scape/Transform;");
	env->SetStaticObjectField(clazz, leftEyeTransformFieldID, leftEyeTransform);
	jobject rightEyeTransform = env->NewObject(transformClass, transformConstructor);
	jfieldID rightEyeTransformFieldID = env->GetStaticFieldID(clazz, "rightEyeTransform", "LNET/worlds/scape/Transform;");
	env->SetStaticObjectField(clazz, rightEyeTransformFieldID, rightEyeTransform);
	// Initializes LibOVR, and the Rift
	ovrInitParams initParams = { ovrInit_RequestVersion | ovrInit_MixedRendering | ovrInit_FocusAware, OVR_MINOR_VERSION, NULL, 0, 0 };
	ovrResult result = ovr_Initialize(&initParams);
	VALIDATE(OVR_SUCCESS(result), "Failed to initialize libOVR.");
	Platform.InitWindow(GetModuleHandle(NULL), L"Oculus Room Tiny (GL)");

	result = ovr_Create(&session, &luid);
	if (!OVR_SUCCESS(result)) {
		jclass runtimeErrorClass = env->FindClass("java/lang/RuntimeException");
		char buffer[612];
		ovrErrorInfo errorInfo;
		ovr_GetLastErrorInfo(&errorInfo);
		snprintf(buffer, 612, "Unable to create OVR session! %d %d: %s", result, errorInfo.Result, errorInfo.ErrorString);
		env->ThrowNew(runtimeErrorClass, buffer);
		return;
	}

	if (Compare(luid, GetDefaultAdapterLuid())) // If luid that the Rift is on is not the default adapter LUID...
	{
		VALIDATE(false, "OpenGL supports only the default graphics adapter.");
	}

	ovrHmdDesc hmdDesc = ovr_GetHmdDesc(session);

	// Setup Window and Graphics
	// Note: the mirror window can be any size, for this sample we use 1/2 the HMD resolution
	ovrSizei windowSize = { hmdDesc.Resolution.w / 2, hmdDesc.Resolution.h / 2 };

	Platform.InitDevice(windowSize.w, windowSize.h, reinterpret_cast<LUID*>(&luid));

	// Make eye render buffers
	for (int eye = 0; eye < 2; ++eye)
	{
		ovrSizei idealTextureSize = ovr_GetFovTextureSize(session, ovrEyeType(eye), hmdDesc.DefaultEyeFov[eye], 1);
		eyeRenderTexture[eye] = new TextureBuffer(session, true, true, idealTextureSize, 1, NULL, 1);
		eyeDepthBuffer[eye] = new DepthBuffer(eyeRenderTexture[eye]->GetSize(), 0);

		if (!eyeRenderTexture[eye]->TextureChain)
		{
			VALIDATE(false, "Failed to create texture.");
		}
	}

	//ovrMirrorTextureDesc desc;
	//memset(&desc, 0, sizeof(desc));
	//desc.Width = windowSize.w;
	//desc.Height = windowSize.h;
	//desc.Format = OVR_FORMAT_R8G8B8A8_UNORM_SRGB;
	//
	//// Create mirror texture and an FBO used to copy mirror texture to back buffer
	//result = ovr_CreateMirrorTextureGL(session, &desc, &mirrorTexture);
	//if (!OVR_SUCCESS(result))
	//{
	//	VALIDATE(false, "Failed to create mirror texture.");
	//}
	//
	//// Configure the mirror read buffer
	//GLuint texId;
	//ovr_GetMirrorTextureBufferGL(session, mirrorTexture, &texId);
	//
	//glGenFramebuffers(1, &mirrorFBO);
	//glBindFramebuffer(GL_READ_FRAMEBUFFER, mirrorFBO);
	//glFramebufferTexture2D(GL_READ_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texId, 0);
	//glFramebufferRenderbuffer(GL_READ_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, 0);
	glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);

	// Turn off vsync to let the compositor do its magic
	wglSwapIntervalEXT(0);

	// FloorLevel will give tracking poses where the floor height is 0
	ovr_SetTrackingOriginType(session, ovrTrackingOrigin_FloorLevel);
	ovr_RecenterTrackingOrigin(session);
	//ovr_SetTrackingOriginType(session, ovrTrackingOrigin_EyeLevel);
	

}

JNIEXPORT void JNICALL Java_NET_worlds_console_VR_vrDestroy(JNIEnv * env, jclass clazz) {
	ovr_Destroy(session);
	ovr_Shutdown();
}

ovrFovPort fovFromHWND(HWND hwnd) {
	ovrFovPort fov;
	fov.DownTan = 1;
	fov.LeftTan = 1;
	fov.UpTan = 1;
	fov.RightTan = 1;
	RECT rcClient;
	GetClientRect(hwnd, &rcClient);
	if (rcClient.bottom - rcClient.top < rcClient.right - rcClient.left) {
		float ratio = (float)(rcClient.bottom - rcClient.top) / (float)(rcClient.right - rcClient.left);
		fov.DownTan = ratio;
		fov.UpTan = ratio;
	}
	else {
		float ratio = (float)(rcClient.right - rcClient.left) / (float)(rcClient.bottom - rcClient.top);
		fov.LeftTan = ratio;
		fov.RightTan = ratio;
	}
	return fov;
}

void rejoinPilotAndCamera(JNIEnv * env, jclass classGlobal) {
	static jmethodID movePilotBy = env->GetStaticMethodID(classGlobal, "movePilotBy", "(FFFF)V");
	ovrTrackingState trackingState = ovr_GetTrackingState(session, 0.0, false);
	ovrPosef pose = trackingState.HeadPose.ThePose;
	float yaw;
	Quatf(pose.Orientation).GetYawPitchRoll(&yaw, NULL, NULL);
	env->CallStaticVoidMethod(classGlobal, movePilotBy, pose.Position.x * SCALE, pose.Position.z * -SCALE, 0.0F, RadToDegree(yaw));
	ovr_RecenterTrackingOrigin(session);
	
}

JNIEXPORT void JNICALL Java_NET_worlds_console_VR_getEyeTransforms(JNIEnv * env, jclass clazz) {


	HWND hwnd[2];
	static jclass classGlobal = (jclass)env->NewGlobalRef(clazz);
	static jfieldID leftHwndFieldID = env->GetStaticFieldID(classGlobal, "leftHWND", "I");
	static jfieldID rightHwndFieldID = env->GetStaticFieldID(classGlobal, "rightHWND", "I");
	hwnd[0] = (HWND)env->GetStaticIntField(classGlobal, leftHwndFieldID);
	hwnd[1] = (HWND)env->GetStaticIntField(classGlobal, rightHwndFieldID);

	ovrSessionStatus sessionStatus;
	ovr_GetSessionStatus(session, &sessionStatus);
	if (sessionStatus.ShouldQuit)
	{
		static jmethodID quitVR = env->GetStaticMethodID(classGlobal, "quitVR", "()V");
		env->CallStaticVoidMethod(classGlobal, quitVR);
		return;
	}
	if (sessionStatus.ShouldRecenter)
		ovr_RecenterTrackingOrigin(session);

	if (sessionStatus.HasInputFocus) {
		ovrInputState inputState;
		ovr_GetInputState(session, ovrControllerType_Active, &inputState);
		if (inputState.Buttons & ovrButton_Enter) {
			rejoinPilotAndCamera(env, classGlobal);
		}
	}

	// Call ovr_GetRenderDesc each frame to get the ovrEyeRenderDesc, as the returned values (e.g. HmdToEyePose) may change at runtime.
	ovrEyeRenderDesc eyeRenderDesc[2];
	eyeRenderDesc[0] = ovr_GetRenderDesc(session, ovrEye_Left, fovFromHWND(hwnd[0]));
	eyeRenderDesc[1] = ovr_GetRenderDesc(session, ovrEye_Right, fovFromHWND(hwnd[1]));

	// Get eye poses, feeding in correct IPD offset
	
	ovrPosef HmdToEyePose[2] = { eyeRenderDesc[0].HmdToEyePose,
		eyeRenderDesc[1].HmdToEyePose };

	ovr_GetEyePoses(session, frameIndex, ovrTrue, HmdToEyePose, EyeRenderPose, &sensorSampleTime);


	ovrPosef ExposedEyeRenderPose[2];

	// Need to convert to centimeter scale
	for (int eye = 0; eye < 2; ++eye) {
		ExposedEyeRenderPose[eye] = EyeRenderPose[eye];
		ExposedEyeRenderPose[eye].Position.x *= SCALE;
		ExposedEyeRenderPose[eye].Position.y *= SCALE;
		ExposedEyeRenderPose[eye].Position.z *= SCALE;

		//ExposedEyeRenderPose[eye].Position.z -= 150;
	}


	jclass transformClassLocal = env->FindClass("NET/worlds/scape/Transform");
	static jclass transformClass = (jclass)env->NewGlobalRef(transformClassLocal);
	static jmethodID transformMakeIdentity = env->GetMethodID(transformClass, "makeIdentity", "()LNET/worlds/scape/Transform;");
	static jmethodID transformMoveTo = env->GetMethodID(transformClass, "moveTo", "(FFF)LNET/worlds/scape/Transform;");
	static jmethodID transformSpin = env->GetMethodID(transformClass, "spin", "(FFFF)LNET/worlds/scape/Transform;");



	static jfieldID leftEyeTransformFieldID = env->GetStaticFieldID(classGlobal, "leftEyeTransform", "LNET/worlds/scape/Transform;");
	static jfieldID rightEyeTransformFieldID = env->GetStaticFieldID(classGlobal, "rightEyeTransform", "LNET/worlds/scape/Transform;");

	static jfieldID transformFieldID[2] = { leftEyeTransformFieldID, rightEyeTransformFieldID };
	for (int eye = 0; eye < 2; ++eye) {
		jobject transform = env->GetStaticObjectField(classGlobal, transformFieldID[eye]);
		transform = env->CallObjectMethod(transform, transformMakeIdentity);
		transform = env->CallObjectMethod(transform, transformMoveTo, ExposedEyeRenderPose[eye].Position.x, -ExposedEyeRenderPose[eye].Position.z, ExposedEyeRenderPose[eye].Position.y - 150);
		//transform = env->CallObjectMethod(transform, transformMoveTo, ExposedEyeRenderPose[eye].Position.x, -ExposedEyeRenderPose[eye].Position.z, ExposedEyeRenderPose[eye].Position.y);
		Vector3f axis;
		float angle;
		Quat<float>(ExposedEyeRenderPose[eye].Orientation).GetAxisAngle(&axis, &angle);
		transform = env->CallObjectMethod(transform, transformSpin, axis.x, -axis.z, axis.y, RadToDegree(angle));
		env->SetStaticObjectField(classGlobal, transformFieldID[eye], transform);
	}
	ovr_WaitToBeginFrame(session, frameIndex);
	ovr_BeginFrame(session, frameIndex);

}

JNIEXPORT void JNICALL Java_NET_worlds_console_VR_submitFrame(JNIEnv * env, jclass clazz) {
	HWND hwnd[2];
	static jclass classGlobal = (jclass)env->NewGlobalRef(clazz);
	static jfieldID leftHwndFieldID = env->GetStaticFieldID(classGlobal, "leftHWND", "I");
	static jfieldID rightHwndFieldID = env->GetStaticFieldID(classGlobal, "rightHWND", "I");
	hwnd[0] = (HWND)env->GetStaticIntField(classGlobal, leftHwndFieldID);
	hwnd[1] = (HWND)env->GetStaticIntField(classGlobal, rightHwndFieldID);

	ovrLayerEyeFov ld;
	ld.Header.Type = ovrLayerType_EyeFov;
	ld.Header.Flags = ovrLayerFlag_TextureOriginAtBottomLeft;   // Because OpenGL.

																// Render Scene to Eye Buffers
	for (int eye = 0; eye < 2; ++eye)
	{
		// Switch to eye render target
		eyeRenderTexture[eye]->SetAndClearRenderSurface(eyeDepthBuffer[eye]);

		ld.ColorTexture[eye] = eyeRenderTexture[eye]->TextureChain;
		ld.Viewport[eye] = Recti(eyeRenderTexture[eye]->GetSize());
		ld.Fov[eye] = fovFromHWND(hwnd[eye]);
		ld.RenderPose[eye] = EyeRenderPose[eye];
		ld.SensorSampleTime = sensorSampleTime;

		CaptureAnImage(hwnd[eye],  &ld.Viewport[eye], &ld.Fov[eye]);

		// Avoids an error when calling SetAndClearRenderSurface during next iteration.
		// Without this, during the next while loop iteration SetAndClearRenderSurface
		// would bind a framebuffer with an invalid COLOR_ATTACHMENT0 because the texture ID
		// associated with COLOR_ATTACHMENT0 had been unlocked by calling wglDXUnlockObjectsNV.
		eyeRenderTexture[eye]->UnsetRenderSurface();

		// Commit changes to the textures so they get picked up frame
		eyeRenderTexture[eye]->Commit();
	}

	// Do distortion rendering, Present and flush/sync


	ovrLayerHeader* layers = &ld.Header;
	ovr_EndFrame(session, frameIndex, nullptr, &layers, 1);
	// exit the rendering loop if submit returns an error, will retry on ovrError_DisplayLost

	frameIndex++;
}


// return true to retry later (e.g. after display lost)
/*static bool MainLoop(bool retryCreate)
{


    // Main loop
    while (Platform.HandleMessages())
    {



        }

        // Blit mirror texture to back buffer
        glBindFramebuffer(GL_READ_FRAMEBUFFER, mirrorFBO);
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
        GLint w = windowSize.w;
        GLint h = windowSize.h;
        glBlitFramebuffer(0, h, w, 0,
                          0, 0, w, h,
                          GL_COLOR_BUFFER_BIT, GL_NEAREST);
        glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);

        SwapBuffers(Platform.hDC);
    }

Done:
    delete roomScene;
    if (mirrorFBO) glDeleteFramebuffers(1, &mirrorFBO);
    if (mirrorTexture) ovr_DestroyMirrorTexture(session, mirrorTexture);
    for (int eye = 0; eye < 2; ++eye)
    {
        delete eyeRenderTexture[eye];
        delete eyeDepthBuffer[eye];
    }
    Platform.ReleaseDevice();
    ovr_Destroy(session);

    // Retry on ovrError_DisplayLost
    return retryCreate || (result == ovrError_DisplayLost);
}*/

//-------------------------------------------------------------------------------------
int WINAPI WinMain(HINSTANCE hinst, HINSTANCE, LPSTR, int)
{
    // Initializes LibOVR, and the Rift
	ovrInitParams initParams = { ovrInit_RequestVersion | ovrInit_FocusAware, OVR_MINOR_VERSION, NULL, 0, 0 };
	ovrResult result = ovr_Initialize(&initParams);
    VALIDATE(OVR_SUCCESS(result), "Failed to initialize libOVR.");

    VALIDATE(Platform.InitWindow(hinst, L"Oculus Room Tiny (GL)"), "Failed to open window.");

    //Platform.Run(MainLoop);

    ovr_Shutdown();

    return(0);
}

void RedrawWorldsWindow(HWND hwnd) {
	// Based on https://stackoverflow.com/questions/3291167/how-can-i-take-a-screenshot-in-a-windows-application
	HDC targetHDC = GetDC(hwnd);
	HDC memHDC = CreateCompatibleDC(targetHDC);
	int width = GetDeviceCaps(targetHDC, HORZRES);
	int height = GetDeviceCaps(targetHDC, VERTRES);
	HBITMAP hBitmap = CreateCompatibleBitmap(targetHDC, width, height);
	BITMAP bitmap;
	SelectObject(memHDC, hBitmap);
	BitBlt(memHDC, 0, 0, width, height, targetHDC, 0, 0, SRCCOPY);
	GetObject(hBitmap, sizeof(BITMAP), &bitmap);
	glDrawPixels(width, height, GL_BGR, GL_UNSIGNED_BYTE, bitmap.bmBits);
	DeleteObject(hBitmap);
	ReleaseDC(hwnd, targetHDC);
	DeleteDC(memHDC);

}

void CaptureAnImage(HWND hWnd, ovrRecti * viewport, ovrFovPort * fov)
{
	UNREFERENCED_PARAMETER(viewport);

	HDC hdcTarget;
	HDC hdcMemDC = NULL;
	HBITMAP hbmTarget = NULL;
	BITMAP bmpTarget;

	// Retrieve the handle to a display device context for the client 
	// area of the window. 
	hdcTarget = GetDC(hWnd);

	// Create a compatible DC which is used in a BitBlt from the window DC
	hdcMemDC = CreateCompatibleDC(hdcTarget);

	if (!hdcMemDC)
	{
		goto done;
	}

	// Get the client area for size calculation
	RECT rcClient;
	GetClientRect(hWnd, &rcClient);

	if (rcClient.bottom - rcClient.top < rcClient.right - rcClient.left) {
		float ratio = (float)(rcClient.bottom - rcClient.top) / (float)(rcClient.right - rcClient.left);
		fov->DownTan = ratio;
		fov->UpTan = ratio;
	}
	else {
		float ratio = (float)(rcClient.right - rcClient.left) / (float)(rcClient.bottom - rcClient.top);
		fov->LeftTan = ratio;
		fov->RightTan = ratio;
	}



	//This is the best stretch mode
	//SetStretchBltMode(hdcMemDC, HALFTONE);

	//The source DC is the entire screen and the destination DC is the current window (HWND)
	/*if (!StretchBlt(hdcMemDC,
		0, 0,
		resolution.w, resolution.h,
		hdcTarget,
		0, 0,
		rcClient.right - rcClient.left,
		rcClient.bottom - rcClient.top,
		SRCCOPY))
	{
		goto done;
	}*/

	// Create a compatible bitmap from the Window DC
	//hbmTarget = CreateCompatibleBitmap(hdcTarget, resolution.w, resolution.h);

	hbmTarget = CreateCompatibleBitmap(hdcTarget, rcClient.right - rcClient.left, rcClient.bottom - rcClient.top);

	if (!hbmTarget)
	{
		goto done;
	}

	// Select the compatible bitmap into the compatible memory DC.
	SelectObject(hdcMemDC, hbmTarget);

	// Bit block transfer into our compatible memory DC.
	if (!BitBlt(hdcMemDC,
		0, 0,
		rcClient.right - rcClient.left, rcClient.bottom - rcClient.top,
		hdcTarget,
		0, 0,
		SRCCOPY))
	{
		goto done;
	}

	viewport->Pos.x = 0;
	//viewport->Pos.y = rcClient.bottom - rcClient.top;

	viewport->Pos.y = viewport->Size.h - (rcClient.bottom - rcClient.top);
	
	viewport->Size.w = rcClient.right - rcClient.left;
	viewport->Size.h = rcClient.bottom - rcClient.top;

	//viewport->Pos.y = 0;
	//viewport->Size.h = 5;

	//if (!StretchBlt(hdcMemDC,
	//	0, 0,
	//	resolution.w, resolution.h,
	//	hdcTarget,
	//	0, 0,
	//	rcClient.right - rcClient.left, rcClient.bottom - rcClient.top,
	//	SRCCOPY))
	//{
	//	goto done;
	//}

	// Get the BITMAP from the HBITMAP
	GetObject(hbmTarget, sizeof(BITMAP), &bmpTarget);

	BITMAPFILEHEADER   bmfHeader;
	UNREFERENCED_PARAMETER(bmfHeader);
	BITMAPINFOHEADER   bi;

	bi.biSize = sizeof(BITMAPINFOHEADER);
	bi.biWidth = bmpTarget.bmWidth;
	bi.biHeight = bmpTarget.bmHeight;
	bi.biPlanes = 1;
	bi.biBitCount = 32;
	bi.biCompression = BI_RGB;
	bi.biSizeImage = 0;
	bi.biXPelsPerMeter = 0;
	bi.biYPelsPerMeter = 0;
	bi.biClrUsed = 0;
	bi.biClrImportant = 0;

	DWORD dwBmpSize = ((bmpTarget.bmWidth * bi.biBitCount + 31) / 32) * 4 * bmpTarget.bmHeight;

	// Starting with 32-bit Windows, GlobalAlloc and LocalAlloc are implemented as wrapper functions that 
	// call HeapAlloc using a handle to the process's default heap. Therefore, GlobalAlloc and LocalAlloc 
	// have greater overhead than HeapAlloc.
	HANDLE hDIB = GlobalAlloc(GHND, dwBmpSize);
	char *lpbitmap = (char *)GlobalLock(hDIB);

	// Gets the "bits" from the bitmap and copies them into a buffer 
	// which is pointed to by lpbitmap.
	GetDIBits(hdcMemDC, hbmTarget, 0,
		(UINT)bmpTarget.bmHeight,
		lpbitmap,
		(BITMAPINFO *)&bi, DIB_RGB_COLORS);

	glDrawPixels(bmpTarget.bmWidth, bmpTarget.bmHeight, GL_BGRA, GL_UNSIGNED_BYTE, lpbitmap);




	
/*
	// A file is created, this is where we will save the screen capture.
	HANDLE hFile = CreateFile("captureqwsx.bmp",
		GENERIC_WRITE,
		0,
		NULL,
		CREATE_ALWAYS,
		FILE_ATTRIBUTE_NORMAL, NULL);

	// Add the size of the headers to the size of the bitmap to get the total file size
	DWORD dwSizeofDIB = dwBmpSize + sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);

	//Offset to where the actual bitmap bits start.
	bmfHeader.bfOffBits = (DWORD)sizeof(BITMAPFILEHEADER) + (DWORD)sizeof(BITMAPINFOHEADER);

	//Size of the file
	bmfHeader.bfSize = dwSizeofDIB;

	//bfType must always be BM for Bitmaps
	bmfHeader.bfType = 0x4D42; //BM   

	DWORD dwBytesWritten = 0;
	WriteFile(hFile, (LPSTR)&bmfHeader, sizeof(BITMAPFILEHEADER), &dwBytesWritten, NULL);
	WriteFile(hFile, (LPSTR)&bi, sizeof(BITMAPINFOHEADER), &dwBytesWritten, NULL);
	WriteFile(hFile, (LPSTR)lpbitmap, dwBmpSize, &dwBytesWritten, NULL);


	//Close the handle for the file that was created
	CloseHandle(hFile);

	*/

	GlobalUnlock(hDIB);
	GlobalFree(hDIB);
	

	//Clean up
done:
	DeleteObject(hbmTarget);
	DeleteObject(hdcMemDC);
	ReleaseDC(hWnd, hdcTarget);

	return;
}