package NET.worlds.console;

import NET.worlds.core.IniFile;
import NET.worlds.scape.Camera;
import NET.worlds.scape.EventQueue;
import NET.worlds.scape.FrameEvent;
import NET.worlds.scape.LibraryDropTarget;
import NET.worlds.scape.Pilot;
import NET.worlds.scape.ToolTipManager;
import NET.worlds.scape.Transform;
import java.awt.Panel;
import java.awt.CheckboxMenuItem;
import java.awt.Canvas;
import java.awt.CardLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Graphics;
import java.awt.IllegalComponentStateException;
import java.awt.Point;
import java.util.Enumeration;
import java.util.Vector;
import NET.worlds.console.VR;

public class RenderCanvas
  extends Canvas
  implements FramePart, LibraryDropTarget
{
  private static final long serialVersionUID = -811200590685352009L;
  DefaultConsole universeConsole;
  private Window w;
  private boolean amIRight = false;
  private boolean vrManager = false;
  private Panel vrPanel;
  private boolean vrEnabled = false;
  private RenderCanvas toTheRight = null;
  private Dimension dim;
  private Camera camera;
  private boolean redidParent = false;
  private int xRenderExtent;
  private int yRenderExtent;
  
  public RenderCanvas(Dimension d)
  {
    this(null, d);
  }
  
  public RenderCanvas(DefaultConsole dc, Dimension d)
  {
    this(dc, d, false);
  }
  
  public RenderCanvas(DefaultConsole dc, Dimension d, boolean right)
  {
    this.amIRight = right;
    this.universeConsole = dc;
    this.dim = d;
    if(!right)
    {
      this.toTheRight = new RenderCanvas(dc, d, true);
    }
    this.overlays = new Vector();
  }
  

  
  
  
  public void drive()
  {
    if ((this.w != null) && (Pilot.getActiveRoom() != null)) {
      this.w.setDeltaMode(true);
    }
  }
  
  public boolean getDeltaMode()
  {
    if (this.w == null) {
      return false;
    }
    return this.w.getDeltaMode();
  }
  
  public void activate(Console c, Container f, Console prev) {}
  
  public void deactivate() {}
  
  public void setCamera(Camera cam)
  {
    this.camera = cam;
    if (cam != null) {
      if(!amIRight) cam.setCanvas(this);
    }
    if(toTheRight != null) toTheRight.setCamera(cam);
  }
  
  public Camera getCamera()
  {
    return this.camera;
  }
  
  public Window getWindow()
  {
    return this.w;
  }
  
  public void update(Graphics g) {}
  
  public void paint(Graphics g) {}
  
  private void enableVR() {
      if(this.toTheRight != null) vrPanel.add(this.toTheRight);
      System.out.println("Enabling VR!");
      VR.vrInit(IniFile.gamma().getIniInt("vrscale", 100));
      vrEnabled = true;
      vrPanel.validate();
      vrPanel.repaint();
  }
  
  private void disableVR() {
      if(this.toTheRight != null) vrPanel.remove(this.toTheRight);
      VR.vrDestroy();
      this.camera.lookAround.makeIdentity();
      vrEnabled = false;
      vrPanel.validate();
      vrPanel.repaint();
  }
  
  public void reshape(int x, int y, int width, int height)
  {
    if(!redidParent && !amIRight && getParent().getLayout().getClass() == CardLayout.class) {
        redidParent = true;
        vrManager = true;
        System.out.println("Component shown!");
        vrPanel = new Panel(new GridLayout(1,2));
        getParent().add("render", vrPanel);
        getParent().remove(this);
        vrPanel.add(this);
        Console.getActive().getMenu("Options").add(VR.checkbox);
    }
    super.reshape(x, y, width, height);
    if(toTheRight != null && vrEnabled) toTheRight.reshape(x, y, width, height);
    this.mayNeedToResize = true;
  }
  
  public Dimension minimumSize()
  {
    return this.dim;
  }
  
  public Dimension preferredSize()
  {
    return this.dim;
  }
  
  public void addOverlay(RenderCanvasOverlay overlay)
  {
    this.overlays.addElement(overlay);
    this.mayNeedToResize = true;
    if(toTheRight != null) toTheRight.addOverlay(overlay);
  }
  
  public void removeOverlay(RenderCanvasOverlay overlay)
  {
    this.overlays.removeElement(overlay);
    this.mayNeedToResize = true;
    if(toTheRight != null) toTheRight.removeOverlay(overlay);
  }
  
  private void computeOverlayDimensions()
  {
    int minYPer;
    int minXPer = minYPer = 100;
    this.fullScreenOverlay = false;
    
    Dimension size = getSize();
    if (this.overlays != null)
    {
      Enumeration e = this.overlays.elements();
      while (e.hasMoreElements())
      {
        RenderCanvasOverlay o = (RenderCanvasOverlay)e.nextElement();
        
        o.canvasResized(size.width, size.height);
        if (o.isFullscreen())
        {
          this.fullScreenOverlay = true;
          minXPer = minYPer = 0;
        }
        int ox = 100 - o.getXPercent();
        int oy = 100 - o.getYPercent();
        if (ox < minXPer) {
          minXPer = ox;
        }
        if (oy < minYPer) {
          minYPer = oy;
        }
      }
    }
    if ((minXPer == 0) && (minYPer == 0))
    {
      this.xRenderExtent = (this.yRenderExtent = 0);
    }
    else if (minXPer == 0)
    {
      this.xRenderExtent = size.width;
      this.yRenderExtent = ((int)(size.height * minYPer * 0.01D));
    }
    else if (minYPer == 0)
    {
      this.xRenderExtent = ((int)(size.width * minXPer * 0.01D));
      this.yRenderExtent = size.height;
    }
    else
    {
      this.xRenderExtent = size.width;
      this.yRenderExtent = size.height;
    }
  }
  
  private boolean fullScreenOverlay = false;
  private boolean mayNeedToResize;
  private Vector overlays;
  
  public boolean handle(FrameEvent f)
  {
    if (this.camera == null) {
      return true;
    }
    if (!checkForWindow(true)) {
      return true;
    }
    EventQueue.pollForEvents(this.camera);
    if (this.w == null) {
      return true;
    }
    if(vrManager && !vrEnabled && VR.checkbox.getState()) {
        enableVR();
    }
    if(vrManager && vrEnabled && !VR.checkbox.getState()) {
        disableVR();
    }
    if (this.mayNeedToResize)
    {
      computeOverlayDimensions();
      
      this.w.maybeResize(this.xRenderExtent, this.yRenderExtent);
      //if (true)
      {
        Point pt = getLocationOnScreen();
        Dimension dim = getSize();
        this.w.reShape(pt.x, pt.y, dim.width, dim.height);
      }
      this.mayNeedToResize = false;
    }
    Transform rightTransform = null;
    if(vrEnabled && toTheRight != null) {
        this.toTheRight.checkForWindow(false);
        VR.leftHWND = this.w.getHwnd();
        VR.rightHWND = this.toTheRight.w.getHwnd();
        VR.getEyeTransforms();
        this.camera.lookAround = VR.leftEyeTransform;
        rightTransform = VR.rightEyeTransform;
    }
    if (this.camera != null) {
      doRender();
    }
    if(vrEnabled && toTheRight != null)
    {
        this.camera.setCanvas(toTheRight);
        this.camera.lookAround = rightTransform;
        toTheRight.handle(f);
        this.camera.setCanvas(this);
        VR.submitFrame();
    }
    return true;
  }
  
  public boolean checkForWindow(boolean interceptEvents)
  {
    if (this.w == null)
    {
      String title = Console.getFrame().getTitle();
      if (title == null) {
        return false;
      }
      try
      {
        this.w = new Window(title, getLocationOnScreen(), getSize(), this.camera, 
          interceptEvents && !amIRight);
        System.out.println("HWND: " + Integer.toString(this.w.getHwnd()));
      }
      catch (IllegalComponentStateException e)
      {
        return false;
      }
      catch (WindowNotFoundException e)
      {
        return false;
      }
      this.mayNeedToResize = false;
    }
    return true;
  }
  
  private synchronized void doRender()
  {
    if ((this.universeConsole != null) && (this.universeConsole.isUniverseMode()))
    {
      ToolTipManager.toolTipManager().killToolTip();
      if (this.w != null) {
        this.w.hideNativeWindow();
      }
      return;
    }
    ToolTipManager.toolTipManager().heartbeat();
    
    this.w.showNativeWindow();
    if (this.fullScreenOverlay)
    {
      ToolTipManager.toolTipManager().killToolTip();
      return;
    }
    this.camera.renderToCanvas();
  }
  
  public void removeNotify()
  {
    if (this.w != null)
    {
      this.w.dispose();
      this.w = null;
    }
    super.removeNotify();
  }
}
