package NET.worlds.console;

import java.awt.CheckboxMenuItem;

import NET.worlds.scape.Transform;
import NET.worlds.console.Main;
import NET.worlds.console.MainCallback;
import NET.worlds.scape.Pilot;
import NET.worlds.scape.Point3Temp;

public class VR {
    static {
        System.loadLibrary("WorldsVR");
    }
    
    
    public static Transform leftEyeTransform;
    public static Transform rightEyeTransform;
    public static int leftHWND;
    public static int rightHWND;
    
    public static CheckboxMenuItem checkbox = new CheckboxMenuItem("Virtual Reality", false);
    
    private static void quitVR() {
        checkbox.setState(false);
    }
    
    public static native void vrInit(int scale);
    
    public static native void vrDestroy();
    
    public static native void getEyeTransforms();

    public static native void submitFrame();
    
    private static void movePilotBy(float dx, float dy, float dz, float yaw) {
        Pilot pilot = Pilot.getActive();
        if(pilot == null) return;
        float currentYaw = pilot.getYaw();
        Point3Temp currentSpinAxis = Point3Temp.make();
        float currentSpin = pilot.getSpin(currentSpinAxis);
        Point3Temp currentPosition = pilot.getPosition();
        
        pilot.makeIdentity()
          .moveTo(currentPosition);
        
        pilot.premoveThrough(Point3Temp.make(dx, dy, dz));
        
        pilot.yaw(yaw);
    }
}